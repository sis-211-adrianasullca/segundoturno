/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.circulo;

/**
 *
 * @author pc
 */
public class Circulo {
    private double radio;
    private int coordenadaX;
    private int coordenadaY;
  
    public Circulo(double radio, int coordenadaX, int coordenadaY) {
        this.radio = radio;
        this.coordenadaX = coordenadaX;
        this.coordenadaY = coordenadaY;
    }
  
    public double getArea() {
        return Math.PI * radio * radio;
    }
  
    public double getPerimetro() {
        return 2 * Math.PI * radio;
    }
  
    public void desplazar(int dx, int dy) {
        coordenadaX += dx;
        coordenadaY += dy;
    }
  
    public void escalar(double factor) {
        radio *= factor;
    }
  
    public static void main(String[] args) {
        Circulo elCirculo = new Circulo(15, 0, 0);
        System.out.println("Área del círculo: " + elCirculo.getArea());
        System.out.println("Perímetro del círculo: " + elCirculo.getPerimetro());
      
        elCirculo.desplazar(0, 0);
        System.out.println("Desplazamiento en X: " + elCirculo.coordenadaX);
        System.out.println("Desplazamiento en Y: " + elCirculo.coordenadaY);
      
        elCirculo.escalar(1.5);
        System.out.println("Escalar: " + elCirculo.radio);
    }
}
